#pragma once

#include <vector>

#include "Input.h"
#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "spriteFont.h"

/*
Animated missile bullet 
Player can only fire one and has to wait for it to leave the 
screen before firing again.
*/

namespace GC
{
	const float SCROLL_SPEED = 10.f;
	const int BGND_LAYERS = 8;
	const float MOUSE_SPEED = 5000;
	const float PAD_SPEED = 500;
	const float ASTEROID_SPEED = 100;
	const float ASTEROID_SPAWN_RATE = 1;
	const float ASTEROID_SPAWN_INC = 1;
	const float ASTEROID_MAX_SPAWN_DELAY = 10;
	const int ASTEROID_SPAWN_TRIES = 50;
	const int ROID_CACHE = 32;
}

struct Bullet
{
	Bullet(MyD3D& d3d)
		:bullet(d3d)
	{}
	Sprite bullet;
	bool active = false;

	void Init(MyD3D& d3d);
	void Render(DirectX::SpriteBatch& batch);
	void Update(float dTime);
	const float MISSILE_SPEED = 300;
};

struct Asteroid
{
	Asteroid(MyD3D& d3d)
		:spr(d3d)
	{}
	Sprite spr;
	bool active = false;	//should it render and animate?

	//setup
	void Init();
	/*
	draw - the atlas has two asteroids, half frames in one, half the other
	asteroids are randomly assigned one and then animate and at random fps
	*/
	void Render(DirectX::SpriteBatch& batch);
	/*
	move left until offscreen, then go inactive
	*/
	void Update(float dTime);
};


//Scrolling with player ship and spawning of asteriods
class PlayMode
{
public:
	PlayMode(MyD3D& d3d);
	void Update(float dTime);
	void Render(float dTime, DirectX::SpriteBatch& batch);
private:
	const float SCROLL_SPEED = 10.f;
	static const int BGND_LAYERS = 8;
	const float SPEED = 250;
	const float MOUSE_SPEED = 5000;
	const float PAD_SPEED = 500;

	MyD3D& mD3D;
	std::vector<Sprite> mBgnd; //parallax layers
	Sprite mPlayer;		//jet
	RECTF mPlayArea;	//don't go outside this	
	Sprite mThrust;		//flames out the back
	std::vector<Asteroid> mAsteroids;	//a cache of asteroids
	float mSpawnRateSec = 1; //how fast to spawn in new asteroids
	float mLastSpawn = 0;	//used in spawn timing
	Bullet mMissile;	//weapon, only one at once
	
	//once we start thrusting we have to keep doing it for 
	//at least a fraction of a second or it looks whack
	float mThrusting = 0; 

	//setup once
	void InitBgnd();
	void InitPlayer();
	void InitRoids();

	//make it move, reset it once it leaves the screen, only one at once
	void UpdateMissile(float dTime);
	//make it scroll parallax
	void UpdateBgnd(float dTime);
	//Spawns in asteroids
	Asteroid* SpawnRoid();
	//move the ship by keyboard, gamepad or mouse
	void UpdateInput(float dTime);
	//make the flames wobble when the ship moves
	void UpdateThrust(float dTime);
	//create a cache of roids to use and re-use
	void UpdateRoids(float dTime);
	//ask each roid if it wants to render
	void RenderRoids(DirectX::SpriteBatch& batch);
	//me - an asteroid to check for collision with all other active ones
	Asteroid* CheckCollRoids(Asteroid& me);
	//vary the spawn rate
	void HandleInput(float dTime);
	//display spawn rate
	void UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font);
};


/*
Basic wrapper for a game
*/
class Game
{
public:
	enum class State { PLAY };
	static MouseAndKeys sMKIn;
	static Gamepads sGamepads;
	State state = State::PLAY;
	Game(MyD3D& d3d);


	void Release();
	void Update(float dTime);
	void Render(float dTime);
private:
	MyD3D& mD3D;
	DirectX::SpriteBatch *mpSB = nullptr;
	//not much of a game, but this is it
	PlayMode mPMode;
};


